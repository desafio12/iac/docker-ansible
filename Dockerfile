# This file is a template, and might need editing before it works on your project.
FROM alpine:latest

# Edit with mysql-client, postgresql-client, sqlite3, etc. for your needs.
# Or delete entirely if not needed.
RUN apk add -U --no-cache ansible openssh py3-yaml
